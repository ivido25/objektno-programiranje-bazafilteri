﻿namespace LV_
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBoxCustomers = new System.Windows.Forms.ComboBox();
            this.customersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.northwindDataSet = new LV_.NorthwindDataSet();
            this.comboBoxShippers = new System.Windows.Forms.ComboBox();
            this.shippersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBoxEmployees = new System.Windows.Forms.ComboBox();
            this.employeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.labelCustomers = new System.Windows.Forms.Label();
            this.labelShippers = new System.Windows.Forms.Label();
            this.labelEmployees = new System.Windows.Forms.Label();
            this.customersTableAdapter = new LV_.NorthwindDataSetTableAdapters.CustomersTableAdapter();
            this.shippersTableAdapter = new LV_.NorthwindDataSetTableAdapters.ShippersTableAdapter();
            this.employeesTableAdapter = new LV_.NorthwindDataSetTableAdapters.EmployeesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.customersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shippersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxCustomers
            // 
            this.comboBoxCustomers.DataSource = this.customersBindingSource;
            this.comboBoxCustomers.DisplayMember = "CompanyName";
            this.comboBoxCustomers.FormattingEnabled = true;
            this.comboBoxCustomers.Location = new System.Drawing.Point(276, 405);
            this.comboBoxCustomers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxCustomers.Name = "comboBoxCustomers";
            this.comboBoxCustomers.Size = new System.Drawing.Size(160, 24);
            this.comboBoxCustomers.TabIndex = 0;
            this.comboBoxCustomers.Text = "None";
            this.comboBoxCustomers.ValueMember = "CustomerID";
            this.comboBoxCustomers.SelectedIndexChanged += new System.EventHandler(this.comboBoxCustomers_SelectedIndexChanged);
            // 
            // customersBindingSource
            // 
            this.customersBindingSource.DataMember = "Customers";
            this.customersBindingSource.DataSource = this.northwindDataSet;
            // 
            // northwindDataSet
            // 
            this.northwindDataSet.DataSetName = "NorthwindDataSet";
            this.northwindDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // comboBoxShippers
            // 
            this.comboBoxShippers.DataSource = this.shippersBindingSource;
            this.comboBoxShippers.DisplayMember = "CompanyName";
            this.comboBoxShippers.FormattingEnabled = true;
            this.comboBoxShippers.Location = new System.Drawing.Point(276, 439);
            this.comboBoxShippers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxShippers.Name = "comboBoxShippers";
            this.comboBoxShippers.Size = new System.Drawing.Size(160, 24);
            this.comboBoxShippers.TabIndex = 1;
            this.comboBoxShippers.Text = "None";
            this.comboBoxShippers.ValueMember = "ShipperID";
            this.comboBoxShippers.SelectedIndexChanged += new System.EventHandler(this.comboBoxShippers_SelectedIndexChanged);
            // 
            // shippersBindingSource
            // 
            this.shippersBindingSource.DataMember = "Shippers";
            this.shippersBindingSource.DataSource = this.northwindDataSet;
            // 
            // comboBoxEmployees
            // 
            this.comboBoxEmployees.DataSource = this.employeesBindingSource;
            this.comboBoxEmployees.DisplayMember = "LastName";
            this.comboBoxEmployees.FormattingEnabled = true;
            this.comboBoxEmployees.Location = new System.Drawing.Point(276, 474);
            this.comboBoxEmployees.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxEmployees.Name = "comboBoxEmployees";
            this.comboBoxEmployees.Size = new System.Drawing.Size(160, 24);
            this.comboBoxEmployees.TabIndex = 2;
            this.comboBoxEmployees.Text = "None";
            this.comboBoxEmployees.ValueMember = "EmployeeID";
            this.comboBoxEmployees.SelectedIndexChanged += new System.EventHandler(this.comboBoxEmployees_SelectedIndexChanged);
            // 
            // employeesBindingSource
            // 
            this.employeesBindingSource.DataMember = "Employees";
            this.employeesBindingSource.DataSource = this.northwindDataSet;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(117, 31);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(912, 341);
            this.dataGridView1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1055, 555);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "b zadatak, insert i delete u orders";
            // 
            // labelCustomers
            // 
            this.labelCustomers.AutoSize = true;
            this.labelCustomers.Location = new System.Drawing.Point(113, 409);
            this.labelCustomers.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCustomers.Name = "labelCustomers";
            this.labelCustomers.Size = new System.Drawing.Size(79, 17);
            this.labelCustomers.TabIndex = 5;
            this.labelCustomers.Text = "Customers:";
            // 
            // labelShippers
            // 
            this.labelShippers.AutoSize = true;
            this.labelShippers.Location = new System.Drawing.Point(113, 443);
            this.labelShippers.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelShippers.Name = "labelShippers";
            this.labelShippers.Size = new System.Drawing.Size(68, 17);
            this.labelShippers.TabIndex = 6;
            this.labelShippers.Text = "Shippers:";
            // 
            // labelEmployees
            // 
            this.labelEmployees.AutoSize = true;
            this.labelEmployees.Location = new System.Drawing.Point(113, 478);
            this.labelEmployees.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelEmployees.Name = "labelEmployees";
            this.labelEmployees.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelEmployees.Size = new System.Drawing.Size(81, 17);
            this.labelEmployees.TabIndex = 7;
            this.labelEmployees.Text = "Employees:";
            // 
            // customersTableAdapter
            // 
            this.customersTableAdapter.ClearBeforeFill = true;
            // 
            // shippersTableAdapter
            // 
            this.shippersTableAdapter.ClearBeforeFill = true;
            // 
            // employeesTableAdapter
            // 
            this.employeesTableAdapter.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1288, 582);
            this.Controls.Add(this.labelEmployees);
            this.Controls.Add(this.labelShippers);
            this.Controls.Add(this.labelCustomers);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.comboBoxEmployees);
            this.Controls.Add(this.comboBoxShippers);
            this.Controls.Add(this.comboBoxCustomers);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.customersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.northwindDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shippersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxCustomers;
        private System.Windows.Forms.ComboBox comboBoxShippers;
        private System.Windows.Forms.ComboBox comboBoxEmployees;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelCustomers;
        private System.Windows.Forms.Label labelShippers;
        private System.Windows.Forms.Label labelEmployees;
        private NorthwindDataSet northwindDataSet;
        private System.Windows.Forms.BindingSource customersBindingSource;
        private NorthwindDataSetTableAdapters.CustomersTableAdapter customersTableAdapter;
        private System.Windows.Forms.BindingSource shippersBindingSource;
        private NorthwindDataSetTableAdapters.ShippersTableAdapter shippersTableAdapter;
        private System.Windows.Forms.BindingSource employeesBindingSource;
        private NorthwindDataSetTableAdapters.EmployeesTableAdapter employeesTableAdapter;
    }
}

