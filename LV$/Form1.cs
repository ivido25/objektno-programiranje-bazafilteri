﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace LV_
{
    public partial class Form1 : Form
    {
        OleDbConnection dbConnection;
        OleDbDataAdapter dbOleAdapter;

        BindingSource bsOrders = new BindingSource();
        

        private const string dbConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Users\ivan25\Documents\Visual Studio 2017\Projects\LV$\LV$\Northwind.MDB";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'northwindDataSet.Employees' table. You can move, or remove it, as needed.
            this.employeesTableAdapter.Fill(this.northwindDataSet.Employees);
            // TODO: This line of code loads data into the 'northwindDataSet.Shippers' table. You can move, or remove it, as needed.
            this.shippersTableAdapter.Fill(this.northwindDataSet.Shippers);
            // TODO: This line of code loads data into the 'northwindDataSet.Customers' table. You can move, or remove it, as needed.
            this.customersTableAdapter.Fill(this.northwindDataSet.Customers);

            
            dataGridView1.DataSource = bsOrders;
           
            
            RefreshData();

            dataGridView1.Columns[0].Visible = false;
            
        }
        //orders
        private void RefreshData()
        {
            string costumer = comboBoxCustomers.SelectedValue.ToString();
            string shipper = comboBoxShippers.SelectedValue.ToString();
            int ship = Int32.Parse(shipper);
            string employee = comboBoxEmployees.SelectedValue.ToString();
            int emp = Int32.Parse(employee);

            using (dbConnection = new OleDbConnection(dbConnectionString))
            {
               dbConnection.Open();

                using (dbOleAdapter = new OleDbDataAdapter())
                {
                    DataSet myDataSet = new DataSet();
                    dbOleAdapter.SelectCommand = new OleDbCommand("SELECT * FROM orders ", dbConnection);
                    dbOleAdapter.Fill(myDataSet, "Orders");

                    bsOrders.DataSource = myDataSet;
                    bsOrders.DataMember = "Orders";

                    dbConnection.Close();
                }
            }
        }
        //
        private void comboBoxCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = comboBoxCustomers.SelectedValue.ToString();
            using (dbConnection = new OleDbConnection(dbConnectionString))
            {
                dbConnection.Open();

                using (dbOleAdapter = new OleDbDataAdapter())
                {
                    DataSet myDataSet = new DataSet();
                    dbOleAdapter.SelectCommand = new OleDbCommand("SELECT * FROM orders WHERE CustomerID='"+selected+"'", dbConnection);
                    dbOleAdapter.Fill(myDataSet, "Orders");

                    bsOrders.DataSource = myDataSet;
                    bsOrders.DataMember = "Orders";

                    dbConnection.Close();
                }
            }
        }
        //
        private void comboBoxEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = comboBoxEmployees.SelectedValue.ToString();
            int sel = Int32.Parse(selected);
           
            using (dbConnection = new OleDbConnection(dbConnectionString))
            {
                dbConnection.Open();

                using (dbOleAdapter = new OleDbDataAdapter())
                {
                    DataSet myDataSet = new DataSet();
                    dbOleAdapter.SelectCommand = new OleDbCommand("SELECT * FROM orders WHERE EmployeeID=" + selected + "", dbConnection);
                    dbOleAdapter.Fill(myDataSet, "Orders");

                    bsOrders.DataSource = myDataSet;
                    bsOrders.DataMember = "Orders";

                    dbConnection.Close();
                }
            }
        }
        //
        private void comboBoxShippers_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = comboBoxShippers.SelectedValue.ToString();
            int sel = Int32.Parse(selected);

            using (dbConnection = new OleDbConnection(dbConnectionString))
            {
                dbConnection.Open();

                using (dbOleAdapter = new OleDbDataAdapter())
                {
                    DataSet myDataSet = new DataSet();
                    dbOleAdapter.SelectCommand = new OleDbCommand("SELECT * FROM orders WHERE ShipVia=" + selected + "", dbConnection);
                    dbOleAdapter.Fill(myDataSet, "Orders");

                    bsOrders.DataSource = myDataSet;
                    bsOrders.DataMember = "Orders";

                    dbConnection.Close();
                }
            }
        }
        //    

    }
}
